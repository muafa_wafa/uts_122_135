package adinata.rohman.mvvmfirebase.user

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail_user.*
import kotlinx.android.synthetic.main.pesanan.view.*
import adinata.rohman.mvvmfirebase.R
import adinata.rohman.mvvmfirebase.map_lokasi

class detail_user : AppCompatActivity() {

    var id =""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_user)
        val aa = intent
        id = aa.getStringExtra("id")
        var nama = aa.getStringExtra("nama")
        var harga = aa.getStringExtra("harga")
        var foto = aa.getStringExtra("imageurl")
        var stock = aa.getStringExtra("stock")
        var status = aa.getStringExtra("status")
        var keterangan = aa.getStringExtra("keterangan")


        namadet.setText(nama)
        hargadet.setText(harga)
        Glide.with(this).load(foto).into(img)
        stokdet.setText(stock)
        keterangandet.setText(keterangan)
        statusdet.setText(status)

        lokasi.setOnClickListener {
            val aa = Intent(this, map_lokasi::class.java)
            startActivity(aa)

        }

        hub.setOnClickListener {
            val viewDial = LayoutInflater.from(this).inflate(R.layout.pesanan,null)
            val dialog  = AlertDialog.Builder(this)
                .setView(viewDial)
            val LihatDial = dialog.show()
            viewDial.namapes.setText(nama)
            viewDial.hargapes.setText(harga)


            viewDial.kirimpes.setOnClickListener {
                var jum = viewDial.jumlahpes.text.toString().toInt()
                var ju = jum*harga.toInt()
                val bel = Intent(Intent.ACTION_VIEW)
                bel.setData(
                    Uri.parse("https://api.whatsapp.com/send?phone=+62895396119349&text=%2AHello%20Kak!%20Terima%20Kasih%20%0D%0ATelah%20Order%0D%0Adi%20TOKO%20KU%2A%0D%0ABerikut%20detail%20pesanan%20Anda%20%3A%0D%0A%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%0D%0ANama%20Barang%20%20%20%20%20%3A%20"+nama+"%0D%0AJumlah%20Barang%20%20%20%3A%20"+viewDial.jumlahpes.text.toString()+"%0D%0AHarga%20Barang%20%20%20%20%3A%20"+harga+"%0D%0A%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%0D%0A_%2ATotal%20Biaya%3A%20Rp.%20"+ju+"%2A_%0D%0A%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%20%20%20%0D%0A%0D%0AJangan%20lupa%20Kak!%0D%0ASiapkan%20uang%20nya%20yaaa%0D%0A%2ATerimakasih%20%3A%29%2A"
                    ))
                startActivity(bel)
                LihatDial.dismiss()
            }
            viewDial.batalpes.setOnClickListener{
                LihatDial.dismiss()
            }

//            var aa ="https://api.whatsapp.com/send?phone=62895396119349&text=~*Hai*%20*"+"TOKO KU"+"*%20~%0A%0APerkenalkan%20%0AMAU%20PESAN%20DONG%20%20%20%3A%20"+nama+"%0AJUMLAH%20%20%20%20%3A%20"+stock+"%0AHarga%20%20%3A%20"+harga+"%0A%0Asaya%20TOTAL%20Bayar%203A%0A"+{var jumlah}+"%0A%0A_Terima%20Kasih_%0A%23TOKO%20KU%20%20"

        }

    }
}
