package muafa.sirojul.uts_122_135

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.messaging.FirebaseMessaging

class splash : AppCompatActivity() {
    private lateinit var iv: ImageView
    private lateinit var ik: TextView
    private lateinit var tv: TextView

    lateinit var pref: SharedPreferences
    var setwar = ""
    val Tag = "openfcm"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        FirebaseMessaging.getInstance().subscribeToTopic("topics/myTopic")

        tv = findViewById(R.id.tv)
        iv = findViewById(R.id.iv)
        ik = findViewById(R.id.tk)
        val set1: Animation = AnimationUtils.loadAnimation(this, R.anim.rcanim)
        val set: Animation = AnimationUtils.loadAnimation(this, R.anim.mytransition)
        tv.startAnimation(set)
        iv.startAnimation(set)
        ik.startAnimation(set)

        pref = getSharedPreferences("setting", Context.MODE_PRIVATE)
        setwar = pref.getString("warna", "").toString()


        val intent = Intent(this, welcome::class.java)
        /*  val handler = Handler()
          handler.postDelayed({

              startActivity(intent)
          }, 5000)
         */

        val tim: Thread
        tim = Thread {
            try {
                Thread.sleep(5000)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            } finally {
                startActivity(intent)
                finish()
            }
        }
        tim.start()
    }

}
